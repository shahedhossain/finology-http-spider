# [finology-http-spider][201]

Most of the java projects of [Shahed, Inc.][000]'s are mavenized, it's a maven `jar` project. This is the child of the [finology-http-parent][202]. This module developed aim to support bootstrap/client application. It will be package as `jar`. The dependent project can be use it as a dependency by following:


```xml
<dependency>
    <artifactId>finology-http-spider</artifactId>
    <groupId>biz.shahed.finology.java.http.spider</groupId>
    <version>1.0.00.GA</version>
</dependency>
```


All of the resources and libraries of [Shahed, Inc.][000] permitted to use under considering `MIT` license. Clone this project using Git Source Control Manager:

1. [`git clone bit.shahed.biz:shahedhossain/finology-http-spider.git`][201] from [bit.shahed.biz][200]


# upload `rsa` keys then add to `~/.ssh/config`

```cfg
Host bit.shahed.biz
     HostName bitbucket.org
     PreferredAuthentications publickey
     IdentityFile ~/.ssh/bit_shahed_rsa
     User git
```

# build

```bash
mvn spring-boot:run
mvn clean install -U
mvn clean install -Dmaven.test.skip
```


# LICENSE

```
Copyright (c) 2013-2020 Shahed, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```


# Project

Practically it's a utility or sample project. `Academian` may use this project under considering `MIT|GPLv3` license. Infact this project started aim to build the foundation of enterprise graded project. Base on this library an enthusiast will be able to build enterprise graded application. This library project will be reduce the learning curve and maximized the focus on productivity.


# Contributor

**Shahed Bin Abdullah** is the founder of **Chorke Academia, Inc.** He is an `Academian` & `Geekery`. In his `sultanate` **Rashida** is his only `sultana` and **Raiyan** is their only `shehzada`. Who are the part of inspiration, innovation and contribution to **Chorke Academia, Inc.**


# Chorke Academia, Inc


**Chorke Academia, Inc** founded aimed to develop `Open Source Enterprise Application` based on `Java`, `JavaScript`, `TypeScript`, `Python`, `Swift`, `Ruby`, `Perl`, `PHP`, `C#` and `C++`. Integrated, Modularized, Simplified and Loosely coupled application development is our vision. Our goal is minimizing the learning curve, maximizing the productivity by using `Open Source Technology`. `Chorke Academia, Inc.` is trying to maintain the best practice for transparent, robust and enrich software development by using `OOP`. Our development process is optimized. What's accelerated by `XP`, `Agile Scrum Master`, `CI` and `CD`.


# Contact

- [**devs@shahed.biz**][100]
- [**shahed.biz**][000]


[000]:  https://shahed.biz "Visit us"
[100]:  mailto:devs@shahed.biz "Email us"

[200]:  https://bitbucket.org/shahedhossain "Shahed"
[201]:  https://bitbucket.org/shahedhossain/finology-http-spider "finology-http-spider"
[202]:  https://bitbucket.org/shahedhossain/finology-http-parent "finology-http-parent"
