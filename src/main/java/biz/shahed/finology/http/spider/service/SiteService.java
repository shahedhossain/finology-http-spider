package biz.shahed.finology.http.spider.service;

import java.util.List;

import biz.shahed.finology.http.spider.entity.Site;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface SiteService {
    Long countAll();
    List<Site> findAll();
    Long countAllPage(int size);
    Site findOneByCode(String code);
    List<Site> findAll(int page, int size);
}