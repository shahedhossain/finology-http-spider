package biz.shahed.finology.http.spider.beans.config;

import java.util.List;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface CrawlerConfig {
    int getNumberOfCrawlers();
    CrawlConfig getCrawlConfig();
    PageFetcher getPageFetcher();
    PageFetcher getPageFetcher(CrawlConfig config);

    RobotstxtConfig getRobotsConfig();
    RobotstxtServer getRobotsServer();
    RobotstxtServer getRobotsServer(CrawlConfig config);
    RobotstxtServer getRobotsServer(CrawlConfig config, RobotstxtConfig robots);
    RobotstxtServer getRobotsServer(RobotstxtConfig robots, PageFetcher fetcher);

    CrawlController getController() throws Exception;
    CrawlController getController(CrawlConfig config) throws Exception;
    CrawlController getController(CrawlConfig config, RobotstxtServer server) throws Exception;
    CrawlController getController(CrawlConfig config, PageFetcher fetcher, RobotstxtServer server) throws Exception;

    CrawlController getController(List<String> seeds) throws Exception;
    CrawlController getController(CrawlConfig config, List<String> seeds) throws Exception;
    CrawlController getController(CrawlConfig config, RobotstxtServer server, List<String> seeds) throws Exception;
    CrawlController getController(CrawlConfig config, PageFetcher fetcher, RobotstxtServer server, List<String> seeds) throws Exception;
    
}
