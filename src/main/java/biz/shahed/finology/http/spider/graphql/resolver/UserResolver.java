package biz.shahed.finology.http.spider.graphql.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;

import biz.shahed.finology.http.spider.entity.Site;
import biz.shahed.finology.http.spider.entity.User;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface UserResolver extends GraphQLResolver<User> {
    public Site getSite(User user);
}
