package biz.shahed.finology.http.spider.beans.parser;

import biz.shahed.finology.http.spider.entity.PageTracker;
import biz.shahed.finology.http.spider.entity.Product;
import edu.uci.ics.crawler4j.crawler.Page;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface ProductPageParser {
    String SELECT_NAME  = "#maincontent > div.columns > div > div.product-info-main > div.page-title-wrapper.product > h1 > span";
    String SELECT_PRICE = "#maincontent > div.columns > div > div.product-info-main > div.product-info-price > div.price-box.price-final_price > span > span > meta:nth-child(3)";
    String ITEM_FORMAT  = "PageURL: %s\n\nName: %s\nPrice: $%.2f\nDescription: %s\nExtra information: %s\n\n";
    String MORE_FORMAT  = "#product-attribute-specs-table > tbody > tr:nth-child(%d) > %s";
    String SELECT_MORE  = "#product-attribute-specs-table > tbody > tr";
    String SELECT_DESC  = "#description > div > div";

    Product parse(Page page, PageTracker tracker);
}
