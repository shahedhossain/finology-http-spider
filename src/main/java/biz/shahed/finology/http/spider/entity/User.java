package biz.shahed.finology.http.spider.entity;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import biz.shahed.finology.http.spider.entity.audit.AuditTrail;
import biz.shahed.finology.http.spider.entity.audit.AuditTrailListener;
import biz.shahed.finology.http.spider.utility.Base36Style;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Entity
@Table(name = "users")
@Access(AccessType.FIELD)
@EntityListeners(AuditTrailListener.class)
public class User implements AuditTrail {
    private static final long serialVersionUID = -543793010469627808L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "enter_at", updatable = false)
    private Date enterAt;

    @Column(name = "enter_by", length = 4, updatable = false)
    private String enterBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "amend_at")
    private Date amendAt;

    @Column(name = "amend_by", length = 4)
    private String amendBy;

    @Version
    @Column(name = "revision", nullable = false)
    private Long revision = 0L;

    @Id
    @Column(name = "code", length = 4)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_sqn")
//  @SequenceGenerator(name = "page_tracker_sqn", sequenceName = "product_sqn", initialValue = 46656, allocationSize = 1)
    @GenericGenerator(name  = "users_sqn", strategy = "biz.shahed.finology.http.spider.utility.Base36Style", parameters = {
        @Parameter(name = Base36Style.INITIAL_VALUE,   value = Base36Style.DIGIT_4_MIN),
        @Parameter(name = Base36Style.SEQUENCE_NAME,   value = "users_sqn"),
        @Parameter(name = Base36Style.ALLOCATION_SIZE, value = "1"),
    })
    private String code;

    @Column(name = "name", length = 64, nullable = false)
    private String name;

    @Column(name = "pass", length = 64, nullable = false)
    private String pass;

    @Column(name = "is_active")
    private Boolean active;

    @Column(name = "is_locked")
    private Boolean locked;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "user_expire_at")
    private Date userExpireAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "pass_expire_at")
    private Date passExpireAt;

    public Date getEnterAt() {
        return enterAt;
    }

    public void setEnterAt(Date enterAt) {
        this.enterAt = enterAt;
    }

    public String getEnterBy() {
        return enterBy;
    }

    public void setEnterBy(String enterBy) {
        this.enterBy = enterBy;
    }

    public Date getAmendAt() {
        return amendAt;
    }

    public void setAmendAt(Date amendAt) {
        this.amendAt = amendAt;
    }

    public String getAmendBy() {
        return amendBy;
    }

    public void setAmendBy(String amendBy) {
        this.amendBy = amendBy;
    }

    public Long getRevision() {
        return revision;
    }

    public void setRevision(Long revision) {
        this.revision = revision;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Boolean getActive() {
        return BooleanUtils.isTrue(active);
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getLocked() {
        return BooleanUtils.isTrue(locked);
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Date getUserExpireAt() {
        return userExpireAt;
    }

    public void setUserExpireAt(Date userExpireAt) {
        this.userExpireAt = userExpireAt;
    }

    public Date getPassExpireAt() {
        return passExpireAt;
    }

    public void setPassExpireAt(Date passExpireAt) {
        this.passExpireAt = passExpireAt;
    }
}