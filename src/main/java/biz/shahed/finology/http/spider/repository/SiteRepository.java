package biz.shahed.finology.http.spider.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import biz.shahed.finology.http.spider.entity.Site;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Repository
public interface SiteRepository extends SiteRepositoryCustom, JpaRepository<Site, String> {
    @Query("SELECT s FROM Site s WHERE s.code = :code")
    Site findOneByCode(@Param("code") String code);

}
