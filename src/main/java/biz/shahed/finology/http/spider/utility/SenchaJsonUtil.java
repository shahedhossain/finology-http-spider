package biz.shahed.finology.http.spider.utility;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class SenchaJsonUtil {
    private static final Logger LOG = LoggerFactory.getLogger(SenchaJsonUtil.class);
    public static final String SENCHA_DATE_FORMATE = "yyyy-MM-dd'T'HH:mm:ss";

    public static String objectAsString(Object object) {
        String html = "";
        try {
            ObjectMapper mapper = getObjectMapper();
            html = mapper.writeValueAsString(object);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return html;
    }

    public static Object stringAsObject(String string, TypeReference<?> type ) {
        ObjectMapper mapper = getObjectMapper();
        try {
            return mapper.readValue(string, type);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }

    public static  ObjectMapper getObjectMapper(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.setDateFormat(new SimpleDateFormat(SENCHA_DATE_FORMATE));
        return mapper;
    }
}

