package biz.shahed.finology.http.spider.service;

import java.util.List;

import biz.shahed.finology.http.spider.entity.Product;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface ProductService {
    Long countAll();
    List<Product> findAll();
    Long countAllPage(int size);
    Product findOneByCode(String code);
    Product save(Product entity);
    Product findOneByName(String name);
    List<Product> findAll(int page, int size);
}