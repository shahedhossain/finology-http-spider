package biz.shahed.finology.http.spider.beans;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import biz.shahed.finology.http.spider.beans.config.CrawlerConfig;
import biz.shahed.finology.http.spider.crawler.ProductCrawler;
import edu.uci.ics.crawler4j.crawler.CrawlController;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class ProductCrawlImpl implements ProductCrawl {
    private static final Logger LOG = LoggerFactory.getLogger(ProductCrawlImpl.class);

    @Autowired
    CrawlerConfig config;

    @Override
    public void crawl(String seed) {
        List<String> seeds = new ArrayList<>();
        seeds.add(seed);
        this.crawl(seeds);
    }

    @Override
    public void crawl(List<String> seeds) {
        try {
            final int numberOfCrawlers = this.config.getNumberOfCrawlers();
            final CrawlController controller = this.config.getController(seeds);
            (new Thread() {
                public void run() {
                    CrawlController.WebCrawlerFactory<ProductCrawler> factory = ProductCrawler::new;
                    controller.start(factory, numberOfCrawlers);
                    controller.waitUntilFinish();
                }
            }).start();
        } catch (Exception e) {
            LOG.warn(e.getMessage(), e);
        }
    }
}
