package biz.shahed.finology.http.spider.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import biz.shahed.finology.http.spider.entity.PageTracker;
import biz.shahed.finology.http.spider.service.PageTrackerService;
import biz.shahed.finology.http.spider.utility.SenchaPagination;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@RestController
@RequestMapping("/api/rest/v1.0/page/tracker")
public class PageTrackerController {
    private static final Logger LOG = LoggerFactory.getLogger(PageTrackerController.class);

    @Autowired
    PageTrackerService service;

    /**
     * http://localhost:1983/crawler/api/rest/v1.0/page/tracker/store?start=0&limit=10
     */
    @GetMapping("store")
    public Map<String, Object>  store(SenchaPagination offset) throws IOException {
        List<PageTracker> data = service.findAll(offset.getPage() - 1, offset.getLimit());
        Map<String, Object> response = new HashMap<String, Object>();
        Long total = service.countAll();

        response.put("message", "success");
        LOG.debug("Response: {}", total);
        response.put("success", true);
        response.put("total", total);
        response.put("data", data);
        return response;
    }
}
