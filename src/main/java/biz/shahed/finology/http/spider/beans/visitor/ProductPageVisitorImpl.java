package biz.shahed.finology.http.spider.beans.visitor;

import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import biz.shahed.finology.http.spider.beans.parser.ProductPageParser;
import biz.shahed.finology.http.spider.entity.PageTracker;
import biz.shahed.finology.http.spider.entity.Product;
import biz.shahed.finology.http.spider.entity.Site;
import biz.shahed.finology.http.spider.service.PageTrackerService;
import biz.shahed.finology.http.spider.service.ProductService;
import biz.shahed.finology.http.spider.service.SiteService;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class ProductPageVisitorImpl implements ProductPageVisitor {
    private static final Logger LOG = LoggerFactory.getLogger(ProductPageVisitorImpl.class);
    private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|jpg|png|mp3|mp4|zip|gz))$");

    @Autowired
    SiteService siteService;

    @Autowired
    ProductService productService;

    @Autowired
    PageTrackerService pageService;

    @Autowired
    ProductPageParser productPageParser;

    @Override
    public boolean willVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        return !FILTERS.matcher(href).matches() && this.hasDomain(href);
    }

    @Override
    public void visit(Page page) {
        String pageURL = page.getWebURL().getURL();
        LOG.debug(pageURL);
        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData data = (HtmlParseData) page.getParseData();
            Set<WebURL> links = data.getOutgoingUrls();

            PageTracker tracker = new PageTracker();
            tracker.setTextMd5Sum(DigestUtils.md5Hex(data.getText()));
            tracker.setHtmlMd5Sum(DigestUtils.md5Hex(data.getHtml()));
            int noOfLinks = !CollectionUtils.isEmpty(links) ? links.size() : 0;
            tracker.setNoOfLinks(noOfLinks);
            tracker.setProductPage(false);
            tracker.setPageURL(pageURL);
            tracker.setParsed(true);

            try {
                Product product = this.productPageParser.parse(page, tracker);
                if (!ObjectUtils.isEmpty(product)) {
                    String message = String.format(ProductPageParser.ITEM_FORMAT, pageURL, product.getName(), product.getPrice(), product.getDetails(), product.getExtraInfo());
                    this.productService.save(product);
                    tracker.setProductPage(true);
                    LOG.info(message);
                }
            } catch (Exception e){
                LOG.error(e.getLocalizedMessage(), e);
            }

            this.pageService.save(tracker);
        }
    }

    private boolean hasDomain(String href) {
        List<Site> sites = this.siteService.findAll();
        if(!CollectionUtils.isEmpty(sites)) {
            for(Site site : sites) {
                if(BooleanUtils.isTrue(site.getActive()) && !ObjectUtils.isEmpty(site.getUrl()) && href.startsWith(site.getUrl())) {
                    return true;
                }
            }
        }
        return false;
    }
}
