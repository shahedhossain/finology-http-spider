package biz.shahed.finology.http.spider.graphql.query;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import biz.shahed.finology.http.spider.beans.ProductCrawl;
import biz.shahed.finology.http.spider.utility.DateUtil;
import biz.shahed.finology.http.spider.utility.Query;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class CrawlerQuery extends Query {
    private static Logger LOG = LoggerFactory.getLogger(CrawlerQuery.class);
    private static String PING_FORMAT = "crawler/crawl/{at => %s, url => %s}";

    @Autowired
    ProductCrawl crawler;

    public String searchCrawl(String url) {
        String time = DateUtil.format(new Date(), DateUtil.ISO_DATE);
        String craw = String.format(PING_FORMAT, time, url);
        crawler.crawl(url);
        LOG.debug(craw);
        return craw;
    }
}
