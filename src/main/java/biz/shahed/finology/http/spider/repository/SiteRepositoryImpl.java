package biz.shahed.finology.http.spider.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Repository
@Transactional(readOnly = true)
public class SiteRepositoryImpl implements SiteRepositoryCustom {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(SiteRepositoryImpl.class);

}
