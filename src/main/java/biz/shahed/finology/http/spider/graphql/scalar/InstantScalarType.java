package biz.shahed.finology.http.spider.graphql.scalar;

import java.time.Instant;
import java.util.Date;

import org.springframework.stereotype.Component;

import biz.shahed.finology.http.spider.utility.DateUtil;
import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class InstantScalarType extends GraphQLScalarType {

    public InstantScalarType() {
        super("Instant", "java.time.Instant", new Coercing<Instant, String>() {

            @Override
            public String serialize(Object dataFetcherResult) throws CoercingSerializeException {
                if(dataFetcherResult != null && dataFetcherResult instanceof Instant) {
                    Instant instant = (Instant)dataFetcherResult;
                    Date date = Date.from(instant);
                    String isoDate = DateUtil.format(date, DateUtil.ISO_DATE);
                    return isoDate;
                }
                return null;
            }

            @Override
            public Instant parseValue(Object input) throws CoercingParseValueException {
                if(input != null && input instanceof String) {
                    String isoDate = (String)input;
                    Date date = DateUtil.parse(isoDate, DateUtil.ISO_DATE);
                    Instant instant = (date != null) ? date.toInstant() : null;
                    return instant;
                }
                return null;
            }

            @Override
            public Instant parseLiteral(Object input) throws CoercingParseLiteralException {
                if(input != null && input instanceof StringValue) {
                    String isoDate = ((StringValue)input).getValue();
                    Date date = DateUtil.parse(isoDate, DateUtil.ISO_DATE);
                    Instant instant = (date != null) ? date.toInstant() : null;
                    return instant;
                }
                return null;
            }
        });
    }
}
