package biz.shahed.finology.http.spider.utility;

import java.io.Serializable;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class Base36Style extends SequenceStyleGenerator {
    private static final Logger LOG = LoggerFactory.getLogger(Base36Style.class);
    public static final String ALLOCATION_SIZE = INCREMENT_PARAM;
    public static final String SEQUENCE_NAME   = SEQUENCE_PARAM;
    public static final String INITIAL_VALUE   = INITIAL_PARAM;
    public static final int    RADIX           = 36;

    public static final String DIGIT_1_MIN = "0";               //            0 =>             Z
    public static final String DIGIT_2_MIN = "36";              //           10 =>            ZZ
    public static final String DIGIT_3_MIN = "1296";            //          100 =>           ZZZ
    public static final String DIGIT_4_MIN = "46656";           //        1,000 =>         Z,ZZZ
    public static final String DIGIT_5_MIN = "1679616";         //       10,000 =>        ZZ,ZZZ
    public static final String DIGIT_6_MIN = "60466176";        //      100,000 =>       ZZZ,ZZZ
    public static final String DIGIT_7_MIN = "2176782336";      //    1,000,000 =>     Z,ZZZ,ZZZ
    public static final String DIGIT_8_MIN = "78364164096";     //   10,000,000 =>    ZZ,ZZZ,ZZZ
    public static final String DIGIT_9_MIN = "2821109907456";   //  100,000,000 =>   ZZZ,ZZZ,ZZZ
    public static final String DIGIT_X_MIN = "101559956668416"; //1,000,000,000 => Z,ZZZ,ZZZ,ZZZ

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        Serializable nextval = super.generate(session, object);
        String base36 = Long.toString((Long) nextval, RADIX);
        LOG.debug("Base36: {}", base36.toUpperCase());
        return base36.toUpperCase();
    }

    @Override
    public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
        super.configure(LongType.INSTANCE, params, serviceRegistry);
    }
}
