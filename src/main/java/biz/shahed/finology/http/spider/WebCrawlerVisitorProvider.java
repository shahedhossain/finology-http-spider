package biz.shahed.finology.http.spider;

import org.springframework.context.ApplicationContext;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface WebCrawlerVisitorProvider {
    WebCrawlerVisitor getVisitor();
    ApplicationContext getContext();
}
