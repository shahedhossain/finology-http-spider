package biz.shahed.finology.http.spider.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import biz.shahed.finology.http.spider.entity.Product;
import biz.shahed.finology.http.spider.repository.ProductRepository;
import biz.shahed.finology.http.spider.utility.OffsetUtil;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Service
@Transactional(readOnly=true)
public class ProductServiceImpl implements ProductService {
    private static final Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    ProductRepository repository;

    @Override
    @Cacheable(value="finology_cache", key = "'get/product/count'")
    public Long countAll() {
        return repository.count();
    }

    @Override
    @Cacheable(value="finology_cache", key = "'get/product'")
    public List<Product> findAll() {
        return repository.findAll();
    }

    @Override
    @Cacheable(value="finology_cache", key="'get/product/page/' + #p0")
    public Long countAllPage(int size) {
        Long noOfRecords = repository.count();
        Long noOfPages   = OffsetUtil.getPageCount(noOfRecords, size);
        return noOfPages;
    }

    @Override
    @Cacheable(value="finology_cache", key="'get/product/' + #p0")
    public Product findOneByCode(String code) {
        return repository.findOneByCode(code);
    }

    @Override
    @Transactional
    @Cacheable(value="finology_cache", key = "'pos/product/' + #p0.name", unless="#result!=null")
    public Product save(Product entity) {
        Product persis = null;
        List<Product> list = repository.findAllByName(entity.getName());
        if(!CollectionUtils.isEmpty(list)) {
            persis = list.get(0);
        }

        if (!ObjectUtils.isEmpty(persis) && !ObjectUtils.isEmpty(persis.getCode())) {
            LOG.debug("Name: {}, Price: {}", persis.getName(), persis.getPrice());
            persis.setName(entity.getName());
            persis.setPrice(entity.getPrice());
            persis.setDetails(entity.getDetails());
            persis.setExtraInfo(entity.getExtraInfo());
            repository.save(persis);
        } else {
            persis = repository.save(entity);
        }
        repository.flush();
        return persis;
    }

    @Override
    @Cacheable(value="finology_cache", key="'get/product/' + #p0")
    public Product findOneByName(String name) {
        Product product = null;
        List<Product> list = repository.findAllByName(name);
        if(!CollectionUtils.isEmpty(list)) {
            product = list.get(0);
        }
        return product;
    }

    @Override
    @Cacheable(value="finology_cache", key="'get/product/page/' + #p0 + '/size/' + #p1")
    public List<Product> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAll(pageable).getContent();
    }
}
