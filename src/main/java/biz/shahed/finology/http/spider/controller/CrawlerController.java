package biz.shahed.finology.http.spider.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import biz.shahed.finology.http.spider.beans.ProductCrawl;
import biz.shahed.finology.http.spider.entity.PageTracker;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@RestController
@RequestMapping("/api/rest/v1.0/crawler")
public class CrawlerController {
    private static final Logger LOG = LoggerFactory.getLogger(CrawlerController.class);

    @Autowired
    ProductCrawl crawler;

    /**
     * <pre>
     * http://localhost:1983/crawler/api/rest/v1.0/crawler
     * {"pageURL": "https://magento-test.finology.com.my/breathe-easy-tank.html"}
     * </pre>
     */
    @PostMapping
    public Map<String, Object>  crawl(@RequestBody PageTracker seed) throws IOException {
        Map<String, Object> response = new HashMap<String, Object>();
        String pageURL = seed.getPageURL();
        this.crawler.crawl(pageURL);
        LOG.debug(pageURL);

        response.put("message", "success");
        response.put("data", pageURL);
        response.put("success", true);
        response.put("total", 0);
        return response;
    }
}
