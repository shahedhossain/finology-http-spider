package biz.shahed.finology.http.spider.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import biz.shahed.finology.http.spider.entity.Product;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Repository
public interface ProductRepository extends ProductRepositoryCustom, JpaRepository<Product, String> {
    @Query("SELECT p FROM Product p WHERE p.code = :code")
    Product findOneByCode(@Param("code") String code);

    @Query("SELECT p FROM Product p WHERE p.name = :name")
    List<Product> findAllByName(@Param("name") String name);
}
