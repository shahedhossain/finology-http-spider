package biz.shahed.finology.http.spider.graphql.scalar;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.stereotype.Component;

import biz.shahed.finology.http.spider.utility.DateUtil;
import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class TimestampScalarType extends GraphQLScalarType {

    public TimestampScalarType() {
        super("Timestamp", "java.sql.Timestamp", new Coercing<Timestamp, String>() {

            @Override
            public String serialize(Object dataFetcherResult) throws CoercingSerializeException {
                if(dataFetcherResult != null && dataFetcherResult instanceof Date) {
                    Timestamp timestamp = (Timestamp)dataFetcherResult;
                    Date date = new Date(timestamp.getTime());
                    String isoDate = DateUtil.format(date, DateUtil.ISO_DATE);
                    return isoDate;
                }
                return null;
            }

            @Override
            public Timestamp parseValue(Object input) throws CoercingParseValueException {
                if(input != null && input instanceof String) {
                    String isoDate = (String)input;
                    Date date = DateUtil.parse(isoDate, DateUtil.ISO_DATE);
                    Timestamp timestamp = (date != null) ? new Timestamp(date.getTime()) : null;
                    return timestamp;
                }
                return null;
            }

            @Override
            public Timestamp parseLiteral(Object input) throws CoercingParseLiteralException {
                if(input != null && input instanceof StringValue) {
                    String isoDate = ((StringValue)input).getValue();
                    Date date = DateUtil.parse(isoDate, DateUtil.ISO_DATE);
                    Timestamp timestamp = (date != null) ? new Timestamp(date.getTime()) : null;
                    return timestamp;
                }
                return null;
            }
        });
    }
}
