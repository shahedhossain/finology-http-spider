package biz.shahed.finology.http.spider.config;

import java.io.File;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Configuration
@ComponentScan(basePackages = { "biz.shahed.finology.http.spider.controller" })
public class SpringWebappConfig implements WebMvcConfigurer  {

    private static final Logger LOG = LoggerFactory.getLogger(SpringWebappConfig.class);
    private String staticContentPath;
    private String strictContentPath;

    @Value("${user.home}/.shahed/finology/var/http/static/content")
    private File staticContentDir;

    @Value("${user.home}/.shahed/finology/var/http/strict/content")
    private File strictContentDir;

    @PostConstruct
    public void init() {
        if(!staticContentDir.exists()) {staticContentDir.mkdirs();}
        if(!strictContentDir.exists()) {strictContentDir.mkdirs();}
        staticContentPath = String.format("file:%s%s", staticContentDir.getAbsoluteFile(), File.separator);
        strictContentPath = String.format("file:%s%s", strictContentDir.getAbsoluteFile(), File.separator);
        LOG.debug("Static Files Dir: {}", staticContentPath);
        LOG.debug("Static Files Dir: {}", strictContentPath);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/content/**").addResourceLocations(staticContentPath);
        registry.addResourceHandler("/strict/content/**").addResourceLocations(strictContentPath);
    }

}
