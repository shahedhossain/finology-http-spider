package biz.shahed.finology.http.spider.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import biz.shahed.finology.http.spider.entity.PageTracker;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Repository
public interface PageTrackerRepository extends PageTrackerRepositoryCustom, JpaRepository<PageTracker, String> {

    @Query("SELECT p FROM PageTracker p WHERE p.code = :code")
    PageTracker findOneByCode(@Param("code") String code);

    @Query("SELECT p FROM PageTracker p WHERE p.pageURL = :pageURL")
    List<PageTracker> findAllByPageURL(@Param("pageURL") String pageURL);
}
