package biz.shahed.finology.http.spider.crawler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import biz.shahed.finology.http.spider.WebCrawlerVisitorProvider;
import biz.shahed.finology.http.spider.beans.config.FinologyContextProvider;
import biz.shahed.finology.http.spider.beans.visitor.ProductPageVisitor;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class ProductCrawler extends WebCrawler implements WebCrawlerVisitorProvider {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(ProductCrawler.class);

    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        return this.getVisitor().willVisit(referringPage, url);
    }

    @Override
    public void visit(Page page) {
        this.getVisitor().visit(page);
    }

    @Override
    public ProductPageVisitor getVisitor() {
        return this.getContext().getBean(ProductPageVisitor.class);
    }

    @Override
    public ApplicationContext getContext() {
        return FinologyContextProvider.getApplicationContext();
    }
}

