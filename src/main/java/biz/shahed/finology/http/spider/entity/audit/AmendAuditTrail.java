package biz.shahed.finology.http.spider.entity.audit;

import java.util.Date;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface AmendAuditTrail extends RevisionAuditTrail {
    public Date getAmendAt();
    public void setAmendAt(Date amendAt);

    public String getAmendBy();
    public void setAmendBy(String amendBy);
}