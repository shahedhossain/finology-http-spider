package biz.shahed.finology.http.spider.utility;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public abstract class BaseUserAuth extends BaseEntity {
    private static final long serialVersionUID = -6321105379531529113L;
    private List<BaseUserRole> authorities = new ArrayList<>();

    private String  userName;
    private String  password;
    private String  roleCode;
    private String  siteCode;
    private String  loginName;
    private boolean activeFlag;
    private boolean lockedFlag;
    private String  userLanguage;
    private Date    userExpiresDate;
    private Date    passExpiresDate;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public boolean isActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    public boolean isLockedFlag() {
        return lockedFlag;
    }

    public void setLockedFlag(boolean lockedFlag) {
        this.lockedFlag = lockedFlag;
    }

    public String getUserLanguage() {
        return userLanguage;
    }

    public void setUserLanguage(String userLanguage) {
        this.userLanguage = userLanguage;
    }

    public Date getUserExpiresDate() {
        return userExpiresDate;
    }

    public void setUserExpiresDate(Date userExpiresDate) {
        this.userExpiresDate = userExpiresDate;
    }

    public Date getPassExpiresDate() {
        return passExpiresDate;
    }

    public void setPassExpiresDate(Date passExpiresDate) {
        this.passExpiresDate = passExpiresDate;
    }

    public List<BaseUserRole> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<BaseUserRole> authorities) {
        this.authorities = authorities;
    }

    public boolean isEnabled(){
        return activeFlag;
    }

    public boolean isAccountNonExpired(){
        boolean isNonExpired= false;
        Date today = new Date();
        if(userExpiresDate != null) {
            isNonExpired = userExpiresDate.after(today);
        }
        return isNonExpired;
    }

    public boolean isCredentialsNonExpired(){
        boolean isNonExpired= false;
        Date today = new Date();
        if(passExpiresDate != null) {
            isNonExpired = passExpiresDate.after(today);
        }
        return isNonExpired;
    }

    public boolean isAccountNonLocked(){
        boolean isNonLocked= false;
        isNonLocked = !lockedFlag;
        return isNonLocked;
    }
}
