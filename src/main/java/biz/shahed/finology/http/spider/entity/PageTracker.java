package biz.shahed.finology.http.spider.entity;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import biz.shahed.finology.http.spider.entity.audit.AuditTrail;
import biz.shahed.finology.http.spider.entity.audit.AuditTrailListener;
import biz.shahed.finology.http.spider.utility.Base36Style;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Entity
@Access(AccessType.FIELD)
@Table(name = "page_tracker")
@EntityListeners(AuditTrailListener.class)
public class PageTracker implements AuditTrail {
    private static final long serialVersionUID = 1472311686719177592L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "enter_at", updatable = false)
    private Date enterAt;

    @Column(name = "enter_by", length = 4, updatable = false)
    private String enterBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "amend_at")
    private Date amendAt;

    @Column(name = "amend_by", length = 4)
    private String amendBy;

    @Version
    @Column(name = "revision", nullable = false)
    private Long revision = 0L;

    @Id
    @Column(name = "code", length = 6)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "page_tracker_sqn")
//  @SequenceGenerator(name = "page_tracker_sqn", sequenceName = "page_tracker_sqn", initialValue = 60466176, allocationSize = 1)
    @GenericGenerator(name  = "page_tracker_sqn", strategy = "biz.shahed.finology.http.spider.utility.Base36Style", parameters = {
        @Parameter(name = Base36Style.INITIAL_VALUE,   value = Base36Style.DIGIT_6_MIN),
        @Parameter(name = Base36Style.SEQUENCE_NAME,   value = "page_tracker_sqn"),
        @Parameter(name = Base36Style.ALLOCATION_SIZE, value = "1"),
    })
    private String code;

    @Column(name = "page_url", length = 2000)
    private String pageURL;

    @Column(name = "text_md5_sum", length = 64, nullable = false)
    private String textMd5Sum;

    @Column(name = "html_md5_sum", length = 64, nullable = false)
    private String htmlMd5Sum;

    @Column(name = "no_of_links", nullable = false)
    private Integer noOfLinks = 0;

    @Column(name = "has_parsed")
    private Boolean parsed;

    @Column(name = "is_product_page")
    private Boolean productPage;

    public Date getEnterAt() {
        return enterAt;
    }

    public void setEnterAt(Date enterAt) {
        this.enterAt = enterAt;
    }

    public String getEnterBy() {
        return enterBy;
    }

    public void setEnterBy(String enterBy) {
        this.enterBy = enterBy;
    }

    public Date getAmendAt() {
        return amendAt;
    }

    public void setAmendAt(Date amendAt) {
        this.amendAt = amendAt;
    }

    public String getAmendBy() {
        return amendBy;
    }

    public void setAmendBy(String amendBy) {
        this.amendBy = amendBy;
    }

    public Long getRevision() {
        return revision;
    }

    public void setRevision(Long revision) {
        this.revision = revision;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPageURL() {
        return pageURL;
    }

    public void setPageURL(String pageURL) {
        this.pageURL = pageURL;
    }

    public String getTextMd5Sum() {
        return textMd5Sum;
    }

    public void setTextMd5Sum(String textMd5Sum) {
        this.textMd5Sum = textMd5Sum;
    }

    public String getHtmlMd5Sum() {
        return htmlMd5Sum;
    }

    public void setHtmlMd5Sum(String htmlMd5Sum) {
        this.htmlMd5Sum = htmlMd5Sum;
    }

    public Integer getNoOfLinks() {
        return noOfLinks;
    }

    public void setNoOfLinks(Integer noOfLinks) {
        this.noOfLinks = noOfLinks;
    }

    public Boolean getParsed() {
        return BooleanUtils.isTrue(parsed);
    }

    public void setParsed(Boolean parsed) {
        this.parsed = parsed;
    }

    public Boolean getProductPage() {
        return BooleanUtils.isTrue(productPage);
    }

    public void setProductPage(Boolean productPage) {
        this.productPage = productPage;
    }
}