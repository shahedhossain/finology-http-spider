package biz.shahed.finology.http.spider.entity.audit;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class AuditTrailListener {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(AuditTrailListener.class);

    @PrePersist
    public void preEntry(EntryAuditTrail audit) {
        audit.setEnterAt(new Date());
        audit.setEnterBy("1000");
    }

    @PreUpdate
    public void preAmend(AmendAuditTrail audit) {
        audit.setAmendAt(new Date());
        audit.setAmendBy("1000");
    }
}