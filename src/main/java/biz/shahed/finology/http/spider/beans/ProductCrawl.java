package biz.shahed.finology.http.spider.beans;

import java.util.List;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface ProductCrawl {
    void crawl(String seed);
    void crawl(List<String> seeds);
}
