package biz.shahed.finology.http.spider.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import biz.shahed.finology.http.spider.entity.Site;
import biz.shahed.finology.http.spider.repository.SiteRepository;
import biz.shahed.finology.http.spider.utility.OffsetUtil;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Service
@Transactional(readOnly=true)
public class SiteServiceImpl implements SiteService {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(SiteServiceImpl.class);

    @Autowired
    SiteRepository repository;

    @Override
    @Cacheable(value="finology_cache", key = "'get/site/count'")
    public Long countAll() {
        return repository.count();
    }

    @Override
    @Cacheable(value="finology_cache", key = "'get/site'")
    public List<Site> findAll() {
        return repository.findAll();
    }

    @Override
    @Cacheable(value="finology_cache", key="'get/site/page/' + #p0")
    public Long countAllPage(int size) {
        Long noOfRecords = repository.count();
        Long noOfPages   = OffsetUtil.getPageCount(noOfRecords, size);
        return noOfPages;
    }

    @Override
    @Cacheable(value="finology_cache", key="'get/site/' + #p0")
    public Site findOneByCode(String code) {
        return repository.findOneByCode(code);
    }

    @Override
    @Cacheable(value="finology_cache", key="'get/site/page/' + #p0 + '/size/' + #p1")
    public List<Site> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAll(pageable).getContent();
    }
}
