package biz.shahed.finology.http.spider.utility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class SenchaPagination implements Serializable {
    private static final Logger LOG = LoggerFactory.getLogger(SenchaPagination.class);
    private static final long serialVersionUID = 5654762437453476470L;

    private int page;
    private int limit;
    private int start;

    private String dir;
    private String sort;
    private String filter;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        LOG.debug("filter: {}", filter);
        this.filter = filter;
    }

    @SuppressWarnings("unchecked")
    public List<SenchaFilter> getSenchaFilters() {
        String filter = getFilter();
        List<SenchaFilter> filters = new ArrayList<SenchaFilter>();
        if (filter != null && !filter.isEmpty()) {
            filters = (List<SenchaFilter>) SenchaJsonUtil.stringAsObject(filter, new TypeReference<ArrayList<SenchaFilter>>(){});
        }
        return filters;
    }

    public Map<String, Object> getFilters() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("limit"    , this.limit);
        map.put("offset"   , this.start);
        map.put("iniOffset", this.start + 1);
        map.put("endOffset", this.page*limit);
        List<SenchaFilter> filters = getSenchaFilters();
        if (filters != null && !filters.isEmpty()) {
            for (SenchaFilter filter : filters) {
                map.put(filter.getProperty(), filter.getValue());
            }
        }
        return map;
    }
    
}