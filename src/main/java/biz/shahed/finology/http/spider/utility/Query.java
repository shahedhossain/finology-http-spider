package biz.shahed.finology.http.spider.utility;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public abstract class Query implements GraphQLQueryResolver {

}
