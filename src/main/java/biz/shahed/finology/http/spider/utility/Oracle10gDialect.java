package biz.shahed.finology.http.spider.utility;

import java.sql.Types;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class Oracle10gDialect extends org.hibernate.dialect.Oracle10gDialect {

    public Oracle10gDialect() {
        registerColumnType(Types.DOUBLE, "float");
    }

}
