package biz.shahed.finology.http.spider.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.oembedler.moon.graphql.boot.SchemaStringProvider;

import biz.shahed.finology.http.spider.utility.GQLSchemaProvider;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Configuration
@ComponentScan({"biz.shahed.finology.http.spider.graphql"})
public class SpringGQLConfig {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(SpringGQLConfig.class);

    @Bean
    public SchemaStringProvider schemaStringProvider() {
        return new GQLSchemaProvider();
    }
}
