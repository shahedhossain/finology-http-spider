package biz.shahed.finology.http.spider.graphql.query;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import biz.shahed.finology.http.spider.entity.Site;
import biz.shahed.finology.http.spider.service.SiteService;
import biz.shahed.finology.http.spider.utility.Query;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class SiteQuery extends Query {
    @SuppressWarnings("unused")
    private static Logger LOG = LoggerFactory.getLogger(SiteQuery.class);

    @Autowired
    SiteService service;

    public Long searchSiteCount() {
        return service.countAll();
    }

    public Long searchSitePageCount(int size) {
        return service.countAllPage(size);
    }

    public List<Site> searchSiteByOffset(int page, int size) {
        return service.findAll(page, size);
    }

    public Site searchSiteByCode(String code) {
        if (!StringUtils.isEmpty(code)) {
            return service.findOneByCode(code);
        }
        return null;
    }
}
