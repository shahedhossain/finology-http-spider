package biz.shahed.finology.http.spider.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import biz.shahed.finology.http.spider.entity.PageTracker;
import biz.shahed.finology.http.spider.repository.PageTrackerRepository;
import biz.shahed.finology.http.spider.utility.OffsetUtil;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Service
@Transactional(readOnly=true)
public class PageTrackerServiceImpl implements PageTrackerService {
    private static final Logger LOG = LoggerFactory.getLogger(PageTrackerServiceImpl.class);

    @Autowired
    PageTrackerRepository repository;

    @Override
    @Cacheable(value="finology_cache", key = "'get/page/tracker/count'")
    public Long countAll() {
        return repository.count();
    }

    @Override
    @Cacheable(value="finology_cache", key = "'get/page/tracker'")
    public List<PageTracker> findAll() {
        return repository.findAll();
    }

    @Override
    @Cacheable(value="finology_cache", key="'get/page/tracker/page/' + #p0")
    public Long countAllPage(int size) {
        Long noOfRecords = repository.count();
        Long noOfPages   = OffsetUtil.getPageCount(noOfRecords, size);
        return noOfPages;
    }

    @Override
    @Cacheable(value="finology_cache", key="'get/page/tracker/' + #p0")
    public PageTracker findOneByCode(String code) {
        return repository.findOneByCode(code);
    }

    @Override
    @Transactional
    @Cacheable(value="finology_cache", key = "'pos/page/tracker/' + #p0.pageURL", unless="#result!=null")
    public PageTracker save(PageTracker entity) {
        PageTracker persis = null;
        List<PageTracker> list = repository.findAllByPageURL(entity.getPageURL());
        if(!CollectionUtils.isEmpty(list)) {
            persis = list.get(0);
        }

        if (!ObjectUtils.isEmpty(persis) && !ObjectUtils.isEmpty(persis.getCode())) {
            LOG.debug("PageURL: {}, Text MD5 Sum: {}", persis.getPageURL(), persis.getTextMd5Sum());
            persis.setParsed(entity.getParsed());
            persis.setProductPage(entity.getProductPage());
            persis.setTextMd5Sum(entity.getTextMd5Sum());
            persis.setHtmlMd5Sum(entity.getHtmlMd5Sum());
            persis.setNoOfLinks(entity.getNoOfLinks());
            repository.save(persis);
        } else {
            persis = repository.save(entity);
        }
        repository.flush();
        return persis;
    }

    @Override
    @Cacheable(value="finology_cache", key="'get/page/tracker/' + #p0")
    public PageTracker findOneByPageURL(String pageURL) {
        PageTracker page = null;
        List<PageTracker> list = repository.findAllByPageURL(pageURL);
        if(!CollectionUtils.isEmpty(list)) {
            page = list.get(0);
        }
        return page;
    }

    @Override
    @Cacheable(value="finology_cache", key="'get/page/tracker/page/' + #p0 + '/size/' + #p1")
    public List<PageTracker> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAll(pageable).getContent();
    }
}
