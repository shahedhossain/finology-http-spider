package biz.shahed.finology.http.spider.beans.visitor;

import biz.shahed.finology.http.spider.WebCrawlerVisitor;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface ProductPageVisitor extends WebCrawlerVisitor {

}
