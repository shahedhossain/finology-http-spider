package biz.shahed.finology.http.spider.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Configuration
@EnableTransactionManagement
@EntityScan("biz.shahed.finology.http.spider.entity")
@EnableJpaRepositories("biz.shahed.finology.http.spider.repository")
public class SpringPersisConfig {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(SpringPersisConfig.class);
    public static final String MAPPING_LOCATIONS = "classpath:/META-INF/hibernate/*.hbm.xml";
}
