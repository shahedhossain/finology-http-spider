package biz.shahed.finology.http.spider;

import edu.uci.ics.crawler4j.crawler.Page;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface WebCrawlerVisitor extends WebCrawlerVisitorVerifier {
    void visit(Page page);
}
