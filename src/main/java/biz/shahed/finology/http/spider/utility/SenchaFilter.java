package biz.shahed.finology.http.spider.utility;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class SenchaFilter implements Serializable {

    private static final long serialVersionUID = 4097157858374268442L;
    
    private String property;
    private Object value;
    
    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        Date date = null;
        SimpleDateFormat formatter;
        if (value != null && value instanceof String) {
            String pattern = SenchaJsonUtil.SENCHA_DATE_FORMATE;
            formatter = new SimpleDateFormat(pattern);
            try {
                date = formatter.parse((String) value);
            } catch (ParseException e) {
                // Not required
            }
        }
        this.value = date != null ? date : value;
    }

}
