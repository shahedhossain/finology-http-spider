package biz.shahed.finology.http.spider.beans.config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class CrawlerSelenuimDriverImpl implements CrawlerSeleniumDriver {
    private static final Logger LOG = LoggerFactory.getLogger(CrawlerSelenuimDriverImpl.class);
    private static final String CHROME_URL_EXP  = "https://chromedriver.storage.googleapis.com/85.0.4183.38/%s";
    private static final String PHANTOM_URL_EXP = "https://docs.medisys.com.my/repo/exe/%s/phantomjs/2.1.1/%s";


    @Value("${finology.http.spider.seleniumDriverBasePath}")
    private String seleniumDriverBasePath;

    @Value("${finology.http.spider.seleniumDriverTempPath}")
    private String seleniumDriverTempPath;

    @Value("${finology.http.spider.enabledChrome}")
    private boolean enabledChrome;


    @Override
    public String getChromeDriverPath() {
        String suffix = "chrome/chromedriver";
        if(SystemUtils.IS_OS_WINDOWS) {
            suffix = String.format("%s.exe", suffix);
        }
        String driverPath = FilenameUtils.concat(this.seleniumDriverBasePath, suffix);
        LOG.debug("Chrome Driver Path: {}", driverPath);
        return driverPath;
    }

    @Override
    public String getChromeDownloadPath() {
        String downloadPath = FilenameUtils.concat(this.seleniumDriverTempPath, "chrome");
        LOG.debug("Chrome Download Path: {}", downloadPath);
        return downloadPath;
    }

    @Override
    public String getPhantomDriverPath() {
        String suffix = "phantom/phantomjs";
        if(SystemUtils.IS_OS_WINDOWS) {
            suffix = String.format("%s.exe", suffix);
        }
        String driverPath = FilenameUtils.concat(this.seleniumDriverBasePath, suffix);
        LOG.debug("Phantom Driver Path: {}", driverPath);
        return driverPath;
    }

    @Override
    public String getPhantomDownloadPath() {
        String downloadPath = FilenameUtils.concat(this.seleniumDriverTempPath, "phantom");
        LOG.debug("Phantom Download Path: {}", downloadPath);
        return downloadPath;
    }

    @Override
    public WebDriver getSeleniumDriver() throws IOException {
        WebDriver driver = null;
        if (enabledChrome && this.downloadChromeDriver()) {
            driver = new ChromeDriver();
        } else if (this.downloadPhantomDriver()){
            driver = new PhantomJSDriver();
        }
        return driver;
    }

    @Override
    @SuppressWarnings("resource")
    public boolean downloadChromeDriver() throws IOException {
        String driverPath = this.getChromeDriverPath();
        File driver = new File(driverPath);

        if(!driver.exists()) {
            String localPath = FilenameUtils.concat(this.getChromeDownloadPath(), "chromedriver.zip");
            File localZip = new File(localPath);

            if (!localZip.exists()) {
                String remoteURL = this.getChromeDriverDownloadAddress();
                LOG.info("Selenium » Chrome Driver » URL: {}", remoteURL);
                URL remoteZip = new URL(remoteURL);
                FileUtils.copyURLToFile(remoteZip, localZip);
                LOG.info("Selenium » Chrome Driver » Downloaded!");
            }

            if(localZip.exists()) {
                ZipFile zip = new ZipFile(localZip);
                if (!driver.exists()) {
                    Enumeration<? extends ZipEntry> entries = zip.entries();
                    while (entries.hasMoreElements()) {
                        ZipEntry entry = entries.nextElement();
                        if (entry.getName().startsWith("chromedriver")){
                            if (!entry.isDirectory()) {
                                driver.getParentFile().mkdirs();
                                InputStream in = zip.getInputStream(entry);
                                OutputStream out = new FileOutputStream(driver);
                                IOUtils.copy(in, out);
                                LOG.info("Selenium » Chrome Driver » Extracted!");

                                driver.setExecutable(true);
                                driver.setWritable(true);
                                driver.setReadable(true);
                                LOG.info("Selenium » Chrome Driver » Executable!");
                            }
                        }
                    }
                }
            }
        }

        System.setProperty("webdriver.chrome.driver", driverPath);
        return driver.exists();
    }

    @Override
    public boolean downloadPhantomDriver() throws IOException {
        String driverPath = this.getPhantomDriverPath();
        File driver = new File(driverPath);

        if(!driver.exists()) {
            String localPath = FilenameUtils.concat(this.getPhantomDownloadPath(), "phantomjs.exe");
            File localExe = new File(localPath);

            if (!localExe.exists()) {
                String remoteURL = this.getPhantomDriverDownloadAddress();
                LOG.info("Selenium » Phantom Driver » URL: {}", remoteURL);
                URL remoteExe = new URL(remoteURL);
                FileUtils.copyURLToFile(remoteExe, localExe);
                LOG.info("Selenium » Phantom Driver » Downloaded!");
            }

            if(localExe.exists()) {
                if(!driver.exists()) {
                    FileUtils.copyFile(localExe, driver);
                    LOG.info("Selenium » Phantom Driver » Extracted!");

                    driver.setExecutable(true);
                    driver.setWritable(true);
                    driver.setReadable(true);
                    LOG.info("Selenium » Phantom Driver » Executable!");
                }
            }
        }

        System.setProperty("phantomjs.binary.path", driverPath);
        return driver.exists();
    }

    private String getChromeDriverDownloadAddress() {
        String suffix = "";
        if(SystemUtils.IS_OS_WINDOWS) {
            suffix = "chromedriver_win32.zip";
        } else if (SystemUtils.IS_OS_MAC_OSX) {
            suffix = "chromedriver_mac64.zip";
        } else if (SystemUtils.IS_OS_LINUX) {
            suffix = "chromedriver_linux64.zip";
        }
        return String.format(CHROME_URL_EXP, suffix);
    }

    private String getPhantomDriverDownloadAddress() {
        String midfix ="", suffix = "";
        if(SystemUtils.IS_OS_WINDOWS) {
            suffix = "phantomjs.exe";
            midfix = "winx32";
        } else if (SystemUtils.IS_OS_MAC_OSX) {
            suffix = "phantomjs";
            midfix = "macx64";
        } else if (SystemUtils.IS_OS_LINUX) {
            suffix = "phantomjs";
            midfix = "linx32";
        }
        return String.format(PHANTOM_URL_EXP, midfix, suffix);
    }
}
