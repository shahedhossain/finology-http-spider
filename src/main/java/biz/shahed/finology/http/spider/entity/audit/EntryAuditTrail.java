package biz.shahed.finology.http.spider.entity.audit;

import java.util.Date;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface EntryAuditTrail extends RevisionAuditTrail {
    public void setEnterAt(Date enterAt);
    public void setEnterBy(String enterBy);
}