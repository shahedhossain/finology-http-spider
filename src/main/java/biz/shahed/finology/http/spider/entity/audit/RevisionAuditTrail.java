package biz.shahed.finology.http.spider.entity.audit;

import java.io.Serializable;
import java.util.Date;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface RevisionAuditTrail extends Serializable {
    public Date getEnterAt();
    public String getEnterBy();

    public String getCode();
    public Long getRevision();
}