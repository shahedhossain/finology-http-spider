package biz.shahed.finology.http.spider.beans.config;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class CrawlerConfigImpl implements CrawlerConfig {
    private static final Logger LOG = LoggerFactory.getLogger(CrawlerConfigImpl.class);

    @Value("${finology.http.spider.resumableCrawling:false}")
    boolean resumableCrawling;

    @Value("${finology.http.spider.maxDepthOfCrawling:2}")
    int maxDepthOfCrawling;

    @Value("${finology.http.spider.crawlUserAgentString}")
    String crawlUserAgentString;

    @Value("${finology.http.spider.politenessDelay:200}")
    int politenessDelay;

    @Value("${finology.http.spider.enabledRobots:false}")
    boolean enabledRobots;

    @Value("${finology.http.spider.crawlStorageFolder}")
    String crawlStorageFolder;

    @Value("${finology.http.spider.maxPagesToFetch:-1}")
    int maxPagesToFetch;

    @Value("${finology.http.spider.numberOfCrawlers:7}")
    int numberOfCrawlers;

    @Override
    public int getNumberOfCrawlers() {
        return this.numberOfCrawlers;
    }

    @Override
    public CrawlConfig getCrawlConfig() {
        CrawlConfig config = new CrawlConfig();
        config.setMaxDepthOfCrawling(this.maxDepthOfCrawling);
        config.setCrawlStorageFolder(this.crawlStorageFolder);
        config.setUserAgentString(this.crawlUserAgentString);
        config.setResumableCrawling(this.resumableCrawling);
        config.setMaxPagesToFetch(this.maxPagesToFetch);
        config.setPolitenessDelay(this.politenessDelay);


//      config.setProxyHost("proxy.shahed.biz");
//      config.setProxyUsername("proxy.user"); 
//      config.setProxyPassword("Proxy.pass");
//      config.setProxyPort(8080);

        LOG.debug("Crawl User Agent: {}", this.crawlUserAgentString);
        return config;
    }

    @Override
    public PageFetcher getPageFetcher() {
        return new PageFetcher(this.getCrawlConfig());
    }

    @Override
    public PageFetcher getPageFetcher(CrawlConfig config) {
        return new PageFetcher(config);
    }

    @Override
    public RobotstxtConfig getRobotsConfig() {
        RobotstxtConfig config = new RobotstxtConfig();
        config.setEnabled(this.enabledRobots);
        return config;
    }

    @Override
    public RobotstxtServer getRobotsServer() {
        return this.getRobotsServer(this.getCrawlConfig());
    }

    @Override
    public RobotstxtServer getRobotsServer(CrawlConfig config) {
        RobotstxtConfig robots = this.getRobotsConfig();
        return this.getRobotsServer(config, robots);
    }

    @Override
    public RobotstxtServer getRobotsServer(CrawlConfig config, RobotstxtConfig robots) {
        return this.getRobotsServer(robots, this.getPageFetcher(config));
    }

    @Override
    public RobotstxtServer getRobotsServer(RobotstxtConfig robots, PageFetcher fetcher) {
        return new RobotstxtServer(robots, fetcher);
    }

    @Override
    public CrawlController getController() throws Exception {
        return this.getController(this.getCrawlConfig());
    }

    @Override
    public CrawlController getController(CrawlConfig config) throws Exception {
        RobotstxtServer server = this.getRobotsServer(config);
        return this.getController(config, server);
    }
    
    @Override
    public CrawlController getController(CrawlConfig config, RobotstxtServer server) throws Exception {
        PageFetcher fetcher = this.getPageFetcher(config);
        return this.getController(config, fetcher, server);
    }

    @Override
    public CrawlController getController(CrawlConfig config, PageFetcher fetcher, RobotstxtServer server) throws Exception {
        return this.getController(config, fetcher, server, new ArrayList<>());
    }

    @Override
    public CrawlController getController(List<String> seeds) throws Exception {
        return this.getController(this.getCrawlConfig(), seeds);
    }

    @Override
    public CrawlController getController(CrawlConfig config, List<String> seeds) throws Exception {
        RobotstxtServer server = this.getRobotsServer(config);
        return this.getController(config, server, seeds);
    }

    @Override
    public CrawlController getController(CrawlConfig config, RobotstxtServer server, List<String> seeds) throws Exception {
        PageFetcher fetcher = this.getPageFetcher(config);
        return this.getController(config, fetcher, server, seeds);
    }

    @Override
    public CrawlController getController(CrawlConfig config, PageFetcher fetcher, RobotstxtServer server, List<String> seeds) throws Exception {
        CrawlController controller = new CrawlController(config, fetcher, server);
        if(!CollectionUtils.isEmpty(seeds)) {
            seeds.forEach(seed->controller.addSeed(seed));
        }
        return controller;
    }
}
