package biz.shahed.finology.http.spider.graphql.query;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import biz.shahed.finology.http.spider.entity.User;
import biz.shahed.finology.http.spider.repository.UserRepository;
import biz.shahed.finology.http.spider.utility.OffsetUtil;
import biz.shahed.finology.http.spider.utility.Query;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class UserQuery extends Query {
    @SuppressWarnings("unused")
    private static Logger LOG = LoggerFactory.getLogger(UserQuery.class);

    @Autowired
    UserRepository repository;

    public Long searchUserCount() {
        return repository.count();
    }

    public Long searchUserPageCount(int size) {
        Long noOfRecords = repository.count();
        Long noOfPages   = OffsetUtil.getPageCount(noOfRecords, size);
        return noOfPages;
    }

    public List<User> searchUserByOffset(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAll(pageable).getContent();
    }

    public User searchUserByCode(String code) {
        if (!StringUtils.isEmpty(code)) {
            return repository.findOneByCode(code);
        }
        return null;
    }
}
