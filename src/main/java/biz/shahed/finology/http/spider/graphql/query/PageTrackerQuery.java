package biz.shahed.finology.http.spider.graphql.query;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import biz.shahed.finology.http.spider.entity.PageTracker;
import biz.shahed.finology.http.spider.service.PageTrackerService;
import biz.shahed.finology.http.spider.utility.Query;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class PageTrackerQuery extends Query {
    @SuppressWarnings("unused")
    private static Logger LOG = LoggerFactory.getLogger(PageTrackerQuery.class);

    @Autowired
    PageTrackerService service;

    public Long searchPageTrackerCount() {
        return service.countAll();
    }

    public Long searchPageTrackerPageCount(int size) {
        return service.countAllPage(size);
    }

    public List<PageTracker> searchPageTrackerByOffset(int page, int size) {
        return service.findAll(page, size);
    }

    public PageTracker searchPageTrackerByPageURL(String pageURL) {
        if (!StringUtils.isEmpty(pageURL)) {
            return service.findOneByPageURL(pageURL);
        }
        return null;
    }

    public PageTracker searchPageTrackerByCode(String code) {
        if (!StringUtils.isEmpty(code)) {
            return service.findOneByCode(code);
        }
        return null;
    }
}
