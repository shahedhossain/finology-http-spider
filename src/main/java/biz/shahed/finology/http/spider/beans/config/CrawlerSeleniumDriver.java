package biz.shahed.finology.http.spider.beans.config;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface CrawlerSeleniumDriver {
    String getChromeDriverPath();
    String getChromeDownloadPath();

    String getPhantomDriverPath();
    String getPhantomDownloadPath();

    WebDriver getSeleniumDriver() throws IOException;
    boolean downloadChromeDriver() throws IOException;
    boolean downloadPhantomDriver() throws IOException;
}
