package biz.shahed.finology.http.spider.service;

import java.util.List;

import biz.shahed.finology.http.spider.entity.PageTracker;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface PageTrackerService {
    Long countAll();
    List<PageTracker> findAll();
    Long countAllPage(int size);
    PageTracker findOneByCode(String code);
    PageTracker save(PageTracker entity);
    PageTracker findOneByPageURL(String pageURL);
    List<PageTracker> findAll(int page, int size);
}