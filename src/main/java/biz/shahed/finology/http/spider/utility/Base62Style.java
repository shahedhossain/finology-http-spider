package biz.shahed.finology.http.spider.utility;

import java.io.Serializable;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class Base62Style extends SequenceStyleGenerator {
    private static final Logger LOG = LoggerFactory.getLogger(Base62Style.class);
    public static final String BASE62_CHAR_SET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String ALLOCATION_SIZE = INCREMENT_PARAM;
    public static final String SEQUENCE_NAME   = SEQUENCE_PARAM;
    public static final String INITIAL_VALUE   = INITIAL_PARAM;
    public static final int    RADIX           = 62;

    public static final String DIGIT_1_MIN = "0";                 //            0 =>             Z
    public static final String DIGIT_2_MIN = "62";                //           10 =>            ZZ
    public static final String DIGIT_3_MIN = "3844";              //          100 =>           ZZZ
    public static final String DIGIT_4_MIN = "238328";            //        1,000 =>         Z,ZZZ
    public static final String DIGIT_5_MIN = "14776336";          //       10,000 =>        ZZ,ZZZ
    public static final String DIGIT_6_MIN = "916132832";         //      100,000 =>       ZZZ,ZZZ
    public static final String DIGIT_7_MIN = "56800235584";       //    1,000,000 =>     Z,ZZZ,ZZZ
    public static final String DIGIT_8_MIN = "3521614606208";     //   10,000,000 =>    ZZ,ZZZ,ZZZ
    public static final String DIGIT_9_MIN = "218340105584896";   //  100,000,000 =>   ZZZ,ZZZ,ZZZ
    public static final String DIGIT_X_MIN = "13537086546263552"; //1,000,000,000 => Z,ZZZ,ZZZ,ZZZ

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        Serializable nextval = super.generate(session, object);
        String base62 = Base62Style.encode((Long) nextval);
        LOG.debug("Base62: {}", base62);
        return base62;
    }

    @Override
    public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
        super.configure(LongType.INSTANCE, params, serviceRegistry);
    }

    public static String encode(long base10) {
        if (base10 < 0) {
            String message = "Base10 Should be Positive";
            LOG.debug(message);
            throw new IllegalArgumentException(message);
        }

        String base62 = "";
        while (base10 > 0) {
            base62  = BASE62_CHAR_SET.charAt((int) (base10 % RADIX)) + base62;
            base10 /= RADIX;
        }
        return base62;
    }

    public static long decode(String base62) {
        for (char character : base62.toCharArray()) {
            if (!BASE62_CHAR_SET.contains(String.valueOf(character))) {
                String message = String.format("Invalid Character(s) in Base62: %c", character);
                LOG.debug(message);
                throw new IllegalArgumentException(message);
            }
        }

        long count  = 1;
        long base10 = 0;
        base62      = new StringBuffer(base62).reverse().toString();
        for (char character : base62.toCharArray()) {
            base10 += BASE62_CHAR_SET.indexOf(character) * count;
            count  *= RADIX;
        }
        return base10;
    }
}
