package biz.shahed.finology.http.spider.graphql.query;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import biz.shahed.finology.http.spider.utility.DateUtil;
import biz.shahed.finology.http.spider.utility.Query;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class RootQuery extends Query {
    private static Logger LOG = LoggerFactory.getLogger(RootQuery.class);
    private static String PING_FORMAT = "ping/search/{at => %s, by => %s}";

    public String searchPing(String name) {
        String time = DateUtil.format(new Date(), DateUtil.ISO_DATE);
        String ping = String.format(PING_FORMAT, time, name);
        LOG.debug(ping);
        return ping;
    }
}
