package biz.shahed.finology.http.spider.graphql.resolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import biz.shahed.finology.http.spider.entity.Site;
import biz.shahed.finology.http.spider.entity.User;
import biz.shahed.finology.http.spider.repository.SiteRepository;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class UserResolverImpl implements UserResolver {

    @Autowired
    SiteRepository siteRepository;

    @Override
    public Site getSite(User user) {
//        String siteCode = user.getSiteCode();
//        if (!StringUtils.isEmpty(siteCode)) {
//            return siteMapper.searchOneByCode(siteCode);
//        }
        return null;
    }
}
