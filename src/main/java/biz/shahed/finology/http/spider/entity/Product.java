package biz.shahed.finology.http.spider.entity;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import biz.shahed.finology.http.spider.entity.audit.AuditTrail;
import biz.shahed.finology.http.spider.entity.audit.AuditTrailListener;
import biz.shahed.finology.http.spider.utility.Base36Style;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Entity
@Table(name = "product")
@Access(AccessType.FIELD)
@EntityListeners(AuditTrailListener.class)
public class Product implements AuditTrail {
    private static final long serialVersionUID = -2598241668007035434L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "enter_at", updatable = false)
    private Date enterAt;

    @Column(name = "enter_by", length = 4, updatable = false)
    private String enterBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "amend_at")
    private Date amendAt;

    @Column(name = "amend_by", length = 4)
    private String amendBy;

    @Version
    @Column(name = "revision", nullable = false)
    private Long revision = 0L;

    @Id
    @Column(name = "code", length = 6)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_sqn")
//  @SequenceGenerator(name = "page_tracker_sqn", sequenceName = "product_sqn", initialValue = 60466176, allocationSize = 1)
    @GenericGenerator(name  = "product_sqn", strategy = "biz.shahed.finology.http.spider.utility.Base36Style", parameters = {
        @Parameter(name = Base36Style.INITIAL_VALUE,   value = Base36Style.DIGIT_6_MIN),
        @Parameter(name = Base36Style.SEQUENCE_NAME,   value = "product_sqn"),
        @Parameter(name = Base36Style.ALLOCATION_SIZE, value = "1"),
    })
    private String code;

    @Column(name = "name", length = 64, nullable = false)
    private String name;

    @Column(name = "price", scale = 6, precision = 2)
    private Double price;

    @Column(name = "details", length = 2048)
    private String details;

    @Column(name = "extra_info", length = 2048)
    private String extraInfo;

    public Date getEnterAt() {
        return enterAt;
    }

    public void setEnterAt(Date enterAt) {
        this.enterAt = enterAt;
    }

    public String getEnterBy() {
        return enterBy;
    }

    public void setEnterBy(String enterBy) {
        this.enterBy = enterBy;
    }

    public Date getAmendAt() {
        return amendAt;
    }

    public void setAmendAt(Date amendAt) {
        this.amendAt = amendAt;
    }

    public String getAmendBy() {
        return amendBy;
    }

    public Long getRevision() {
        return revision;
    }

    public void setRevision(Long revision) {
        this.revision = revision;
    }

    public void setAmendBy(String amendBy) {
        this.amendBy = amendBy;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }
}