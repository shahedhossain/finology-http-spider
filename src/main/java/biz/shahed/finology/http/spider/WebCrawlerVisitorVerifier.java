package biz.shahed.finology.http.spider;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface WebCrawlerVisitorVerifier {
    boolean willVisit(Page referringPage, WebURL url);
}
