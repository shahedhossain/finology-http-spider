package biz.shahed.finology.http.spider.utility;

import org.hibernate.MappingException;
import org.hibernate.dialect.identity.IdentityColumnSupportImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class SQLiteIdentityColumnSupport extends IdentityColumnSupportImpl {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(SQLiteDialect.class);

    @Override
    public boolean supportsIdentityColumns() {
        return true;
    }
 
    @Override
    public String getIdentitySelectString(String table, String column, int type) 
      throws MappingException {
        return "select last_insert_rowid()";
    }
 
    @Override
    public String getIdentityColumnString(int type) throws MappingException {
        return "integer";
    }
}