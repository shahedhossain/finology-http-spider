package biz.shahed.finology.http.spider.graphql.mutation;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import biz.shahed.finology.http.spider.utility.DateUtil;
import biz.shahed.finology.http.spider.utility.Mutation;


/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class RootMutation extends Mutation {
    private static Logger LOG = LoggerFactory.getLogger(RootMutation.class);
    private static String PING_FORMAT = "ping/insert/{at => %s, by => %s}";

    public String insertPing(String name) {
        String time = DateUtil.format(new Date(), DateUtil.ISO_DATE);
        String ping = String.format(PING_FORMAT, time, name);
        LOG.info(ping);
        return ping;
    }
}
