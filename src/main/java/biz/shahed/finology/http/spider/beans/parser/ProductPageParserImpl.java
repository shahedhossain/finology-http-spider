package biz.shahed.finology.http.spider.beans.parser;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import biz.shahed.finology.http.spider.beans.config.CrawlerSeleniumDriver;
import biz.shahed.finology.http.spider.entity.PageTracker;
import biz.shahed.finology.http.spider.entity.Product;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.parser.ParseData;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class ProductPageParserImpl implements ProductPageParser {
    private static final Logger LOG = LoggerFactory.getLogger(ProductPageParserImpl.class);

    @Value("${finology.http.spider.enabledChrome}")
    private boolean enabledChrome;

    @Autowired
    CrawlerSeleniumDriver seleniumDriver;

    @Override
    public Product parse(Page page, PageTracker tracker) {
        Product product = null;
        try {
            ParseData parseData = page.getParseData();
            if (!ObjectUtils.isEmpty(parseData)) {
                HtmlParseData data = (HtmlParseData) parseData;
                if (!ObjectUtils.isEmpty(data)) {
                    Document html = Jsoup.parse(data.getHtml());
                    if (!ObjectUtils.isEmpty(html)) {
                        product = this.mining(html);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return product;
    }

    @SuppressWarnings("unused")
    private Product mining(Document html) {
        Element name  = html.select(ProductPageParser.SELECT_NAME).first();
        Element price = html.select(ProductPageParser.SELECT_PRICE).first();
        Element desc  = html.select(ProductPageParser.SELECT_DESC).first();
        Elements list = html.select(ProductPageParser.SELECT_MORE);
        List<String> extra = new ArrayList<String>();

        int index = 1;
        for(Element e : list) {
            String th = String.format(ProductPageParser.MORE_FORMAT, index, "th");
            String td = String.format(ProductPageParser.MORE_FORMAT, index, "td");
            if (!ObjectUtils.isEmpty(html.select(th)) && !ObjectUtils.isEmpty(html.select(td))) {
                String property = String.format("%s:%s", html.select(th).text(), html.select(td).text());
                extra.add(property);
            }
            index++;
        }

        Product product = null;
        if (!ObjectUtils.isEmpty(name) && !ObjectUtils.isEmpty(price) && !ObjectUtils.isEmpty(desc)) {
            product = new Product();
            product.setName(name.text());
            product.setDetails(desc.text());
            product.setExtraInfo(String.join("|", extra));
            product.setPrice(Double.parseDouble(price.attr("content")));
        }
        return product;
    }
}
