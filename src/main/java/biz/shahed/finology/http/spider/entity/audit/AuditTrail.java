package biz.shahed.finology.http.spider.entity.audit;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface AuditTrail extends EntryAuditTrail, AmendAuditTrail {

}