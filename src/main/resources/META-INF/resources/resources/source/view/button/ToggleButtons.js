Ext.define('Web.view.button.ToggleButtons', {
    extend: 'Web.view.button.grid.PageTrackerGrid',
    xtype: 'toggle-buttons',
    controller: 'buttons',
    requires: [
        'Web.view.button.grid.PageTrackerGrid'
    ],
    width: '500',

    //<example>
    otherContent: [{
        type: 'Controller',
        path: 'resources/source/view/button/ButtonsController.js'
    }],
    profiles: {
        classic: {
            width: 420
        },
        neptune: {
            width: 475
        },
        triton: {
            width: 500
        },
        'neptune-touch': {
            width: 585
        }
    }
});