Ext.define('Web.view.button.ButtonsController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.buttons',

    toggleDisabled: function (checkbox, checked) {
        var view = this.getView(),
            stateFn = checked ? 'disable' : 'enable',
            buttons = view.query('button');

        Ext.each(buttons, function (btn) {
            btn[stateFn]();
        });
    },
    onClickCrawlButton     : function(btn, evt) {
         var me            = this,
             refs          = me.getReferences(),
             crawlButton   = refs.crawlButton,
             pageURL       = me.getViewModel().get('pageURL'),
             crawler      = new Web.model.Crawler({
                pageURL: pageURL
            });
        
        crawler.save();
        Ext.MessageBox.alert('Status', 'Request is Processing!');
    }
});
