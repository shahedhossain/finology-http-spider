Ext.define('Web.view.button.MenuButtons', {
    extend: 'Web.view.button.grid.ProductGrid',
    xtype: 'menu-buttons',
    controller: 'buttons',
    requires: [
        'Web.view.button.grid.ProductGrid'
    ],
    width: '500',

    //<example>
    otherContent: [{
        type: 'Controller',
        path: 'resources/source/view/button/ButtonsController.js'
    }],
    profiles: {
        classic: {
            width: 470
        },
        neptune: {
            width: 570
        },
        triton: {
            width: 590
        },
        'neptune-touch': {
            width: 670
        }
    }
});