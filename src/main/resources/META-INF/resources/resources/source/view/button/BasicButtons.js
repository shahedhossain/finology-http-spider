Ext.define('Web.view.button.BasicButtons', {
    extend: 'Web.view.button.crawl.CrawlerView',
    xtype: 'basic-buttons',
    controller: 'buttons',
    cls: 'button-view',
    width: '500',

    //<example>
    otherContent: [{
        type: 'Controller',
        path: 'resources/source/view/button/ButtonsController.js'
    }],
    profiles: {
        classic: {
            width: 420
        },
        neptune: {
            width: 475
        },
        triton: {
            width: 500
        },
        'neptune-touch': {
            width: 585
        }
    }
});