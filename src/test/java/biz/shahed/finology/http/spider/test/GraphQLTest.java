package biz.shahed.finology.http.spider.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class GraphQLTest {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(GraphQLTest.class);

    @BeforeEach
    public void setUp() throws Exception {
        //TODO
    }

    @Test
    public void testDefault() throws Exception {
        Assertions.assertTrue(true);
    }
}
