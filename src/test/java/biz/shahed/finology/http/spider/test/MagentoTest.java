package biz.shahed.finology.http.spider.test;

import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import biz.shahed.boot.BootstrapApplication;
import biz.shahed.finology.http.spider.beans.config.CrawlerConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BootstrapApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class MagentoTest {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(MagentoTest.class);

    CrawlController controller ;

    @Autowired
    CrawlerConfig config;

    @BeforeEach
    public void setUp() throws Exception {
        CrawlController controller  = this.config.getController();
        controller.addSeed("https://magento-test.finology.com.my/breathe-easy-tank.html");
        this.controller = controller;
    }

//    @Test
    public void testCrawler() throws Exception {
        CrawlController.WebCrawlerFactory<MagentoCrawler> factory = MagentoCrawler::new;
        this.controller.start(factory, this.config.getNumberOfCrawlers());
        Thread.sleep(5 * 1000);

        this.controller.shutdown();
        this.controller.waitUntilFinish();
        Assertions.assertTrue(true);
    }

    @Test
    public void testDefault() throws Exception {
        Assertions.assertTrue(true);
    }
}

class MagentoCrawler extends WebCrawler {
    private static final Logger LOG = LoggerFactory.getLogger(MagentoCrawler.class);
    private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|jpg|png|mp3|mp4|zip|gz))$");
    private static final String DOMAIN = "https://magento-test.finology.com.my/";

    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        return !FILTERS.matcher(href).matches() && href.startsWith(DOMAIN);
    }

    @Override
    public void visit(Page page) {
        LOG.info("URL: {}", page.getWebURL().getURL());
        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData data = (HtmlParseData) page.getParseData();
            Set<WebURL> links = data.getOutgoingUrls();
            String html = data.getHtml();
            String text = data.getText();
            LOG.info("Length   >>Text: {}, HTML: {}, Links: {}", text.length(), html.length(), links.size());
            LOG.info("MD5 Sum>>Text: {}, HTML: {}", DigestUtils.md5Hex(text), DigestUtils.md5Hex(html));
        }
    }
}
