package biz.shahed.finology.http.spider.test.service;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.ObjectUtils;

import biz.shahed.boot.BootstrapApplication;
import biz.shahed.finology.http.spider.entity.PageTracker;
import biz.shahed.finology.http.spider.service.PageTrackerService;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BootstrapApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class PageTrackerServiceTest {
    private static final Logger LOG = LoggerFactory.getLogger(PageTrackerServiceTest.class);

    @Autowired
    PageTrackerService service;

    @Test
    public void testPageTracker() throws Exception {
        PageTracker entity = new PageTracker();
        entity.setPageURL("https://magento-test.finology.com.my/promotions/tees-all.html?eco_collection=0");
        entity.setTextMd5Sum("3f756d59ad0a570c273da1517a1c7368");
        entity.setHtmlMd5Sum("494f77218bec4e4b9dea4caf0683b792");
        this.service.save(entity);

        PageTracker page = this.service.findOneByCode("100000");
        if (!ObjectUtils.isEmpty(page)) {
            LOG.info("PageURL: {}, Text MD5 Sum: {}", page.getPageURL(), page.getTextMd5Sum());
        }
        Assertions.assertNotNull(page.getPageURL());
    }

//    @Test
    public void testOffsetQuery() throws Exception {
        List<PageTracker> list = this.service.findAll(0, 10);
        if (list.size() > 0) {
            PageTracker page = list.get(0);
            LOG.info("PageURL: {}, Text MD5 Sum: {}", page.getPageURL(), page.getTextMd5Sum());
        }
        Assertions.assertEquals(0, list.size());
    }

    @Test
    public void testDefault() throws Exception {
        Assertions.assertTrue(true);
    }
}
