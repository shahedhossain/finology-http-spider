package biz.shahed.finology.http.spider.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.github.bonigarcia.seljup.SeleniumExtension;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@ExtendWith(SeleniumExtension.class)
public class ProductTest {
    private static final Logger LOG = LoggerFactory.getLogger(ProductTest.class);
    private static final String SEED_URL = "https://magento-test.finology.com.my/breathe-easy-tank.html";
    private static final String XPATH_NAME    = "/html/body/div/main[@id='maincontent']/div[@class='columns']/div/div/div/h1/span[@class='base']";
    private static final String XPATH_PRICE   = "/html/body/div/main/div/div/div/div[@class='product-info-price']/div/span[@class='normal-price']/span/meta[@itemprop='price']";
    private static final String XPATH_EXTRA   = "/html/body/div/main[@id='maincontent']/div[@class='columns']/div/div[contains(@class, 'product') and contains(@class, 'info') and contains(@class, 'detailed')]/div/div[4]/div/table/tbody/tr";
    private static final String XPATH_DETAILS = "/html/body/div/main[@id='maincontent']/div[@class='columns']/div/div[contains(@class, 'product') and contains(@class, 'info') and contains(@class, 'detailed')]/div/div/div/div[@class='value']";

//    @Test
    void testWithChrome(ChromeDriver driver) {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(SEED_URL);
        this.mining(driver);
    }

    @Test
    void testWithPhamtomJS(PhantomJSDriver driver) {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(SEED_URL);
        this.mining(driver);
    }

    private void mining(WebDriver driver) {
        LOG.info("Title: {}", driver.getTitle());
        String name    = driver.findElement(By.xpath(XPATH_NAME)).getText();
        String details = driver.findElement(By.xpath(XPATH_DETAILS)).getText();
        String price   = driver.findElement(By.xpath(XPATH_PRICE)).getAttribute("content");

        int size = driver.findElements(By.xpath(XPATH_EXTRA)).size();
        List<String> more = new ArrayList<String>();
        for(int index = 1; index <= size; index++) {
            String thXPath = String.format("%s[%d]/th[contains(@class, 'col') and contains(@class, 'label')]", XPATH_EXTRA, index);
            String tdXPath = String.format("%s[%d]/td[contains(@class, 'col') and contains(@class, 'data')]", XPATH_EXTRA, index);
            LOG.debug("\nxpath » th »» {}\nxpath » td »» {}", thXPath, tdXPath);

            String thText = driver.findElement(By.xpath(thXPath)).getText();
            String tdText = driver.findElement(By.xpath(tdXPath)).getText();
            String keyVal = String.format("%s:%s", thText, tdText);
            more.add(keyVal);
        }

        String extra = String.join("|", more);
        LOG.info("Name: {}, Price: {}, Details: {},", name, price, details);
        LOG.info("Extra» {}", extra);
    }
}
