package biz.shahed.finology.http.spider.test;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.github.bonigarcia.seljup.BrowserType;
import io.github.bonigarcia.seljup.DockerBrowser;
import io.github.bonigarcia.seljup.DriverCapabilities;
import io.github.bonigarcia.seljup.DriverUrl;
import io.github.bonigarcia.seljup.SeleniumExtension;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@ExtendWith(SeleniumExtension.class)
public class SeleniumTest {
    private static final Logger LOG = LoggerFactory.getLogger(SeleniumTest.class);

//    @Test
    void testWithChrome(ChromeDriver driver) {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://shahed.biz");
        LOG.info("Title: {}",driver.getTitle());
    }

//    @Test //FIXME
    void testWithHtmlUnit(HtmlUnitDriver driver) {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://shahed.biz");
        LOG.info("Title: {}",driver.getTitle());
    }

    @Test
    void testWithPhamtomJS(PhantomJSDriver driver) {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://shahed.biz");
        LOG.info("Title: {}",driver.getTitle());
    }

//    @Test //FIXME
    void testWithRemoteChrome(
            @DriverUrl("http://localhost:4444/wd/hub")
            @DriverCapabilities("browserName=chrome")
            RemoteWebDriver driver) {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://shahed.biz");
        LOG.info("Title: {}",driver.getTitle());
    }

//    @Testt //FIXME
    void testAndroid(
            @DockerBrowser(type = BrowserType.ANDROID, version = "9.0")
            RemoteWebDriver driver) {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://shahed.biz");
        LOG.info("Title: {}",driver.getTitle());
    }
}
