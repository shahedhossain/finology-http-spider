package biz.shahed.finology.http.spider.test;

import java.util.Set;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import biz.shahed.boot.BootstrapApplication;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BootstrapApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class CrawlerTest {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(CrawlerTest.class);

    @Value("${finology.http.spider.crawlStorageFolder}")
    String crawlStorageFolder;

    @Value("${finology.http.spider.numberOfCrawlers:7}")
    int numberOfCrawlers;

    CrawlController controller ;

    @BeforeEach
    public void setUp() throws Exception {
        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(this.crawlStorageFolder);

        PageFetcher fetcher = new PageFetcher(config);
        RobotstxtConfig robotConfig = new RobotstxtConfig();
        RobotstxtServer robotServer = new RobotstxtServer(robotConfig, fetcher);
        CrawlController controller  = new CrawlController(config, fetcher, robotServer);

        controller.addSeed("https://www.ics.uci.edu/~lopes/");
        controller.addSeed("https://www.ics.uci.edu/~welling/");
        controller.addSeed("https://www.ics.uci.edu/");
        this.controller = controller;
    }

//    @Test
    public void testCrawler() throws Exception {
        CrawlController.WebCrawlerFactory<ICSCrawler> factory = ICSCrawler::new;
        this.controller.start(factory, this.numberOfCrawlers);
        Thread.sleep(5 * 1000);

        this.controller.shutdown();
        this.controller.waitUntilFinish();
        Assertions.assertTrue(true);
    }

    @Test
    public void testDefault() throws Exception {
        Assertions.assertTrue(true);
    }
}

class ICSCrawler extends WebCrawler {
    private static final Logger LOG = LoggerFactory.getLogger(ICSCrawler.class);
    private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|jpg|png|mp3|mp4|zip|gz))$");
    private static final String DOMAIN = "https://www.ics.uci.edu/";

    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        return !FILTERS.matcher(href).matches() && href.startsWith(DOMAIN);
    }

    @Override
    public void visit(Page page) {
        LOG.info("URL: {}", page.getWebURL().getURL());
        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData data = (HtmlParseData) page.getParseData();
            Set<WebURL> links = data.getOutgoingUrls();
            String html = data.getHtml();
            String text = data.getText();
            LOG.info("Length>>Text: {}, HTML: {}, Links: {}", text.length(), html.length(), links.size());
        }
    }
}
