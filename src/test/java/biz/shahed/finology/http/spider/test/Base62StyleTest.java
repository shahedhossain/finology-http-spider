package biz.shahed.finology.http.spider.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.finology.http.spider.utility.Base62Style;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class Base62StyleTest {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(Base62StyleTest.class);

    @BeforeEach
    public void setUp() throws Exception {
        //TODO
    }

    @Test
    public void testEncode() throws Exception {
        Assertions.assertEquals(Base62Style.encode(61L), "Z");
        Assertions.assertEquals(Base62Style.encode(3843L), "ZZ");
        Assertions.assertEquals(Base62Style.encode(238327L), "ZZZ");
        Assertions.assertEquals(Base62Style.encode(14776335L), "ZZZZ");
        Assertions.assertEquals(Base62Style.encode(916132831L), "ZZZZZ");
        Assertions.assertEquals(Base62Style.encode(56800235583L), "ZZZZZZ");
        Assertions.assertEquals(Base62Style.encode(3521614606207L), "ZZZZZZZ");
        Assertions.assertEquals(Base62Style.encode(218340105584895L), "ZZZZZZZZ");
        Assertions.assertEquals(Base62Style.encode(839299365868340223L), "ZZZZZZZZZZ");
    }

    @Test
    public void testDecode() throws Exception {
        Assertions.assertEquals(Base62Style.decode("0"), 0L);
        Assertions.assertEquals(Base62Style.decode("10"), 62L);
        Assertions.assertEquals(Base62Style.decode("100"), 3844L);
        Assertions.assertEquals(Base62Style.decode("1000"), 238328L);
        Assertions.assertEquals(Base62Style.decode("10000"), 14776336L);
        Assertions.assertEquals(Base62Style.decode("100000"), 916132832L);
        Assertions.assertEquals(Base62Style.decode("1000000"), 56800235584L);
        Assertions.assertEquals(Base62Style.decode("10000000"), 3521614606208L);
        Assertions.assertEquals(Base62Style.decode("100000000"), 218340105584896L);
        Assertions.assertEquals(Base62Style.decode("1000000000"), 13537086546263552L);

        Assertions.assertEquals(Base62Style.decode("z"), 35L);
        Assertions.assertEquals(Base62Style.decode("zz"), 2205L);
        Assertions.assertEquals(Base62Style.decode("zzz"), 136745L);
        Assertions.assertEquals(Base62Style.decode("zzzz"), 8478225L);
        Assertions.assertEquals(Base62Style.decode("zzzzz"), 525649985L);
        Assertions.assertEquals(Base62Style.decode("zzzzzz"), 32590299105L);
        Assertions.assertEquals(Base62Style.decode("zzzzzzz"), 2020598544545L);
        Assertions.assertEquals(Base62Style.decode("zzzzzzzz"), 125277109761825L);
        Assertions.assertEquals(Base62Style.decode("zzzzzzzzz"), 7767180805233185L);
        Assertions.assertEquals(Base62Style.decode("zzzzzzzzzz"), 481565209924457505L);

        Assertions.assertEquals(Base62Style.decode("Z"), 61L);
        Assertions.assertEquals(Base62Style.decode("ZZ"), 3843L);
        Assertions.assertEquals(Base62Style.decode("ZZZ"), 238327L);
        Assertions.assertEquals(Base62Style.decode("ZZZZ"), 14776335L);
        Assertions.assertEquals(Base62Style.decode("ZZZZZ"), 916132831L);
        Assertions.assertEquals(Base62Style.decode("ZZZZZZ"), 56800235583L);
        Assertions.assertEquals(Base62Style.decode("ZZZZZZZ"), 3521614606207L);
        Assertions.assertEquals(Base62Style.decode("ZZZZZZZZ"), 218340105584895L);
        Assertions.assertEquals(Base62Style.decode("ZZZZZZZZZ"), 13537086546263551L);
        Assertions.assertEquals(Base62Style.decode("ZZZZZZZZZZ"), 839299365868340223L);
    }
}
