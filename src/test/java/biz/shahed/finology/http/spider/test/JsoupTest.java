package biz.shahed.finology.http.spider.test;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import biz.shahed.finology.http.spider.beans.parser.ProductPageParser;
import biz.shahed.finology.http.spider.entity.Product;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class JsoupTest {
    private static final Logger LOG = LoggerFactory.getLogger(JsoupTest.class);

    Document html = null;

    @BeforeEach
    public void setUp() throws Exception {
        Resource resource = new ClassPathResource("/META-INF/product/breathe_easy_tank.html");
        String html = IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);
        this.html = Jsoup.parse(html);
        LOG.debug(html);
    }

    @Test
    @SuppressWarnings("unused")
    public void testMining() throws Exception {
        Element name  = this.html.select(ProductPageParser.SELECT_NAME).first();
        Element price = this.html.select(ProductPageParser.SELECT_PRICE).first();
        Element desc  = this.html.select(ProductPageParser.SELECT_DESC).first();
        Elements list = this.html.select(ProductPageParser.SELECT_MORE);
        List<String> extra = new ArrayList<String>();

        int index = 1;
        for(Element e : list) {
            String th = String.format(ProductPageParser.MORE_FORMAT, index, "th");
            String td = String.format(ProductPageParser.MORE_FORMAT, index, "td");
            String property = String.format("%s:%s", this.html.select(th).text(), this.html.select(td).text());
            extra.add(property);
            index++;
        }

        Product product = new Product();
        product.setName(name.text());
        product.setDetails(desc.text());
        product.setExtraInfo(String.join("|", extra));
        product.setPrice(Double.parseDouble(price.attr("content")));
        String pageURL ="https://magento-test.finology.com.my/breathe-easy-tank.html";
        String message = String.format(ProductPageParser.ITEM_FORMAT, pageURL, product.getName(), product.getPrice(), product.getDetails(), product.getExtraInfo());
        LOG.info(message);
    }
}
