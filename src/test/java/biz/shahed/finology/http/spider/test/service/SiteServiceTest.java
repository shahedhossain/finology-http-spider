package biz.shahed.finology.http.spider.test.service;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.ObjectUtils;

import biz.shahed.boot.BootstrapApplication;
import biz.shahed.finology.http.spider.entity.Site;
import biz.shahed.finology.http.spider.service.SiteService;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BootstrapApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class SiteServiceTest {
    private static final Logger LOG = LoggerFactory.getLogger(SiteServiceTest.class);

    @Autowired
    SiteService service;

    @Test
    public void testSite() throws Exception {
        Site site = this.service.findOneByCode("1000");
        if (!ObjectUtils.isEmpty(site)) {
            LOG.info("Code :{}, Name :{}", site.getCode(), site.getName());
        }
        Assertions.assertNotNull(site);
    }

    @Test
    public void testOffsetQuery() throws Exception {
        List<Site> list = this.service.findAll(0, 10);
        if (list.size() > 0) {
            Site site = list.get(0);
            LOG.info("Code :{}, Name :{}", site.getCode(), site.getName());
        }
        Assertions.assertEquals(1, list.size());
    }

    @Test
    public void testDefault() throws Exception {
        Assertions.assertTrue(true);
    }
}
