package biz.shahed.finology.http.spider.test.beans;

import org.apache.commons.lang3.SystemUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import biz.shahed.boot.BootstrapApplication;
import biz.shahed.finology.http.spider.beans.config.CrawlerSeleniumDriver;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BootstrapApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class CrawlerSeleniumDriverTest {
    private static final Logger LOG = LoggerFactory.getLogger(CrawlerSeleniumDriverTest.class);

    @Autowired
    CrawlerSeleniumDriver seleniumDriver;

    @Test
    public void testChromeDriverPath() throws Exception {
        String suffix = ".shahed/finology/var/selenium/driver/chrome/chromedriver";
        String path   = seleniumDriver.getChromeDriverPath();
        if(SystemUtils.IS_OS_WINDOWS) {
            suffix = String.format("%s.exe", suffix);
        }
        LOG.debug("Chrome Driver Path: {}", path );
        Assertions.assertTrue(path.endsWith(suffix));
    }

    @Test
    public void testChromeDownloadPath() throws Exception {
        String suffix = ".shahed/finology/tmp/selenium/driver/chrome";
        String path   = seleniumDriver.getChromeDownloadPath();
        Assertions.assertTrue(path.endsWith(suffix));
    }

    @Test
    public void testPhantomDriverPath() throws Exception {
        String suffix = ".shahed/finology/var/selenium/driver/phantom/phantomjs";
        String path = seleniumDriver.getPhantomDriverPath();
        if(SystemUtils.IS_OS_WINDOWS) {
            suffix = String.format("%s.exe", suffix);
        }
        LOG.debug("Phantom Driver Path: {}", path );
        Assertions.assertTrue(path.endsWith(suffix));
    }

    @Test
    public void testPhantomDownloadPath() throws Exception {
        String suffix = ".shahed/finology/tmp/selenium/driver/phantom";
        String path = seleniumDriver.getPhantomDownloadPath();
        Assertions.assertTrue(path.endsWith(suffix));
    }

    @Test
    public void testDownloadChromeDriver() throws Exception {
        boolean hasDownloaded = seleniumDriver.downloadChromeDriver();
        Assertions.assertTrue(hasDownloaded);
    }

    @Test
    public void testDownloadPhantomDriver() throws Exception {
        boolean hasDownloaded = seleniumDriver.downloadPhantomDriver();
        Assertions.assertTrue(hasDownloaded);
    }

    @Test
    public void testDefault() throws Exception {
        Assertions.assertTrue(true);
    }
}
